<?php
    $errors = '';
    $myemail = 'sconchaq@gmail.com';
    if(empty($_POST['name']) ||
       empty($_POST['ciudad'])  ||
       empty($_POST['email'])  ||
       empty($_POST['telefono']) ||
       empty($_POST['message']))
    {
    $errors .= "\n Error: campo obligatorio";
    }
    $name = $_POST['name'];
    $ciudad = $_POST['ciudad'];
    $email = $_POST['email'];
    $telefono = $_POST['telefono'];
    $message = $_POST['message'];
    if( empty($errors)){
        $to = $myemail;
        $email_budget = "Un nuevo pedido de oración en espera: {$name}";
        $email_body='
        Has recibido un nuevo pedido de oración. Los detalles se dan a continuación.
        Email: '.$email.'
        Nombre: '.$name.'
        Ciudad: '.$ciudad.'
        Telefono: '.$telefono.'
        Pedido de Oración: '.$message.'';

        $headers = "De: " . $email. "\r\n";
        $headers .= "Responder a: ". strip_tags($_POST['req-email']) . "\r\n";
       
        mail($to, $email_budget, $email_body, $headers);
    }
?>
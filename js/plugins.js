
/* ---------------------------------------------
    Contact Form Js
 --------------------------------------------- */
 $(function() {
    $('form#contact_form').submit(function(e) {
        e.preventDefault(); 
        $('form#contact_form .error').remove();
        var hasError = false;
        var $name = $('form input[id="form_name"]');
        var $ciudad = $('form input[id="form_ciudad"]'); 
        var $email = $('form input[id="form_email"]'); 
        var $telefono = $('form input[id="form_telefono"]'); 
        var $message = $('form textarea[id="form_message"]'); 
        var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        if ($email.val() == '' || !re.test($email.val())){
            $email.parent().append('</br></br> - <span style="color:red;">Debes ingresar un email valido.</span>');
            $email.addClass('inputError');
            hasError = true;
        }
        if($name.val() == '') {
            $name.parent().append('</br> - <span style="color:red;">Por favor escribe tu nombre.</span>');
            $name.addClass('inputError');
            hasError = true;
        }
        if($ciudad.val()=='') {
            $ciudad.parent().append('</br> - <span style="color:red;">Por favor escribe tu ciudad.</span>');
            $ciudad.addClass('inputError');
            hasError = true;
        }

        if($telefono.val() == ''){
            $telefono.parent().append('</br> - <span style="color:red;">Por favor escribe tu telefono.</span>');
            $telefono.addClass('inputError');
            hasError = true;
        }

        if($message.val() == '') {
            $message.parent().append('</br> - <span style="color:red;">Por favor escribe tu pedido de oración.</span>');
            $message.addClass('inputError');
            hasError = true;
        }
        if(!hasError) {
            var url = "./php/contact-form.php"; 
            $.ajax({
               type: "POST",
               url: url,
               data: $("#contact_form").serialize()
            })
            .done(function(response) {
                // Make sure that the formMessages div has the 'success' class.
                $('form#contact_form').removeClass('error');
                $('form#contact_form').addClass('success');

                // Set the message text.
                $('form#contact_form > .row').slideUp(300);
                $('form#contact_form').prepend('</br></br><span style="color:green;">Hemos recibido tu pedido de oracion. Estaremos orando. 🙏</span></br></br>');

                // Clear the form.
                $('form input[name="email"], form input[name="name"], form input[name="ciudad"], form input[name="telefono"], form textarea[name="message"]').val('');
            })
            .fail(function(data) {
                // Make sure that the formMessages div has the 'error' class.
                $('form#contact_form').removeClass('success');
                $('form#contact_form').addClass('error');

                // Set the message text.
                if (data.responseText !== '') {
                    $('form#contact_form').text(data.responseText);
                } else {
                    $('#snackbar').text('Oops! Se produjo un error y tu mensaje no pudo ser enviado.');
                }
            });
        }
       return false;
    });
});